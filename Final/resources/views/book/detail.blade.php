@extends('master')
@section('judul')
    Halaman Detail Book
@endsection
@section('content')
<a href="/book" class="btn btn-sm btn-secondary my-3">kembali</a>

<div class="row">
        <div class="card">
            <img src="{{asset ('image/'. $book->image)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h4 class="card-title">{{$book->title}}</h4>
              <p class="card-text">{{$book->summary}}</p>
            </div>
          </div>
</div>    
@endsection
