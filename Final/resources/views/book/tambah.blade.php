@extends('master')
@section('judul')
    Halaman Tambah Book
@endsection
@section('content')
<form method="POST" enctype="multipart/form-data" action="/book">
    @csrf
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="form-group">
      <label >Title</label>
      <input type="text" name="title" class="form-control">
    </div>
    
    <div class="form-group">
        <label >Summary</label>
        <textarea class="form-control" name="summary" cols="30" rows="10"></textarea>
      </div>

      <div class="form-group">
        <label >Image</label>
        <input type="file" name="image" class="form-control">
      </div>

      <div class="form-group">
        <label >Author</label>
        <select name="author_id" id="">
            <option value="">--Pilih Author--</option>
            @forelse ($author as $item)
            <option value='{{$item->id}}'>{{$item->name}}</option>
            @empty
            <option value="">--Tidak Ada Author--</option>
            @endforelse
        </select>
      </div>
      
      <button type="submit" class="btn btn-primary">Upload</button>
      
  </form>
@endsection
