@extends('master')
@section('judul')
    Halaman Edit Book
@endsection
@section('content')
<form method="POST" enctype="multipart/form-data" action="/book/{{$book->id}}">
    @method('put')
    @csrf
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    
    
    <div class="form-group">
      <label >Title</label>
      <input type="text" value="{{$book->title}}" name="title" class="form-control">
    </div>
    <div class="form-group">
        <label >Summary</label>
        <textarea class="form-control" name="summary" cols="30" rows="10">{{$book->summary}}</textarea>
      </div>
      
      <div class="form-group">
        <label >Image</label>
        <input type="file" name="image" class="form-control">
      </div>
      
      <div class="form-group">
        <label >Author</label>
        <select name="author_id" id="">
            <option value="">--Pilih Author--</option>
            @forelse ($author as $item)
                @if ($item->id===$book->author_id)
                <option value="{{$item->id}}"selected>{{$item->name}}</option>

                @else
                <option value="{{$item->id}}">{{$item->name}}</option>
                @endif
                
            <option value='{{$item->id}}'>{{$item->name}}</option>
            @empty
            <option value="">--Tidak Ada Author--</option>
            @endforelse
        </select>
      </div>
      
      <button type="submit" class="btn btn-primary">Upload</button>
      
  </form>
@endsection
