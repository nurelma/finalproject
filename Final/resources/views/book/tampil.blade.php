@extends('master')
@section('judul')
    Halaman Tampil Book
@endsection
@section('content')

@auth
<a href="/book/create" class="btn btn-sm btn-primary my-3">Tambah</a>
@endauth

<div class="row">
    @forelse ($book as $item)
    <div class="col=20">
        <div class="card">
            <img src="{{asset ('image/'. $item->image)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h4 class="card-title">{{$item->title}}</h4>
              <p class="card-text">{{Str::limit($item->summary,50,'...')}}</p>
              <a href="/book/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
              

              @auth
              <div class="row my-2">
                <div class="col">
                {{--edit--}}
                <a href="/book/{{$item->id}}/edit" class="btn btn-warning btn-block">Edit</a>
              </div>
              <div class="col">
                {{--delete--}}
                <form action="/book/{{$item->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <button type="submit" class="btn btn-danger btn-block">Delete</a>
                  </form>
              </div>
            </div>
            @endauth


            </div>
          </div>
    @empty
    <h4>Belum Tampil</h4>
    @endforelse
    

</div>    
@endsection
