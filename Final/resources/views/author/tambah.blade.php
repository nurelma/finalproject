@extends('master')
@section('judul')
    Halaman Tambah Author
@endsection
@section('content')
<form method="POST" action="/author">
    @csrf
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <div class="form-group">
      <label >Author Name</label>
      <input type="text" name="name" class="form-control">
    </div>
    <div class="form-group">
      <label >Nationality</label>
      <input type="integer" name="nationality" class="form-control">
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection