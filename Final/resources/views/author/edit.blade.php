@extends('master')
@section('judul')
    Halaman Edit Author
@endsection
@section('content')
<form method="POST" action="/author/{{$author->id}}">
    @csrf
    @method('PUT')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    
    <div class="form-group">
      <label >Author Name</label>
      <input type="text" name="name"  value={{$author->name}} class="form-control">
    </div>
    <div class="form-group">
      <label >Nationality</label>
      <input type="integer" name="nationality" value={{$author->nationality}} class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
