@extends('master')
@section('judul')
    Halaman Tampil Author
@endsection
@section('content')
<a href="/author/create"class="btn btn-primary btn-sm my-4">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Nationality</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($author as $index => $item)
        <tr>
            <th scope="row">{{$index + 1}} </th>
            <td>{{$item->name}}</td>
            <td>{{$item->nationality}}</td>
            <td>
               <form action="/author/{{$item->id}}" method="POST">
                  <a href="/author/{{$item->id}} " class= "btn btn-info btn-sm">Detail</a>
                  <a href="/author/{{$item->id}}/edit" class="btn btn-secondary btn-sm">Edit</a>
                  @csrf
                  @method('delete')
                  <input type="submit" clas="btn btn-danger btn-sm" value="Delete">  
                </form>
            </td>
        </tr>
        @empty
            <p>Author kosong silahkan input</p>
        @endforelse
    </tbody>
  </table>
@endsection