@extends('master')
@section('judul')
    Halaman Edit Load
@endsection
@section('content')
<form method="POST" action="/load/{{$load->id}}">
    @csrf
    @method('PUT')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <div class="form-group">
        <label >Email</label>
        <select name="users_id" id="">
            <option value="">--Pilih Email--</option>
            @forelse ($users as $item)
                @if ($item->id===$load->users_id)
                <option value="{{$item->id}}"selected>{{$item->email}}</option>

                @else
                <option value="{{$item->id}}">{{$item->email}}</option>
                @endif
                
            <option value='{{$item->id}}'>{{$item->email}}</option>
            @empty
            <option value="">--Tidak Ada Author--</option>
            @endforelse
        </select>
      </div>

      <div class="form-group">
        <label >Book</label>
        <select name="book_id" id="">
            <option value="">--Pilih Book--</option>
            @forelse ($book as $item)
                @if ($item->id===$load->book_id)
                <option value="{{$item->id}}"selected>{{$item->title}}</option>

                @else
                <option value="{{$item->id}}">{{$item->title}}</option>
                @endif
                
            <option value='{{$item->id}}'>{{$item->title}}</option>
            @empty
            <option value="">--Tidak Ada Book--</option>
            @endforelse
        </select>
      </div>

      <button type="submit" class="btn btn-primary">Upload</button>
@endsection
