@extends('master')
@section('judul')
    Halaman Tampil Load
@endsection
@section('content')
<a href="/load/create"class="btn btn-primary btn-sm my-4">Tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Email</th>
        <th scope="col">Book</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($load as $index => $item)
        <tr>
            <th scope="row">{{$index + 1}} </th>
            <td>{{$item->users_id}}</td>
            <td>{{$item->book_id}}</td>
            <td>
               <form action="/load/{{$item->id}}" method="POST">
                  <a href="/load/{{$item->id}}/edit" class="btn btn-secondary btn-sm">Edit</a>
                  @csrf
                  @method('delete')
                  <input type="submit" clas="btn btn-danger btn-sm" value="Delete">  
                </form>
            </td>
        </tr>
        @empty
            <p>Load kosong silahkan input</p>
        @endforelse
    </tbody>
  </table>
@endsection