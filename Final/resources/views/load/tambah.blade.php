@extends('master')
@section('judul')
    Halaman Tambah Load
@endsection
@section('content')
<form method="POST" action="/load">
    @csrf
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
    <div class="form-group">
        <label >Nama email</label>
        <select name="users_id" id="">
            <option value="">--Pilih email--</option>
            @forelse ($users as $item)
            <option value='{{$item->id}}'>{{$item->email}}</option>
            @empty
            <option value="">--Tidak Ada email--</option>
            @endforelse
        </select>
      </div>

      <div class="form-group">
        <label >Book</label>
        <select name="book_id" id="">
            <option value="">--Pilih Book--</option>
            @forelse ($book as $item)
            <option value='{{$item->id}}'>{{$item->title}}</option>
            @empty
            <option value="">--Tidak Ada book--</option>
            @endforelse
        </select>
      </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection