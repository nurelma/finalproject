<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $author = DB::table('author')->get();
        return view('author.tampil', ['author' => $author]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('author.tambah');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'nationality' => 'required',
            
        ]);
    

    //simpan data ke database
    DB::table('author')->insert([
        'name' => $request->input('name'),
        'nationality' => $request->input('nationality'),
        
    ]);

     //arahan ke halaman tampil semua cast
     return redirect('/author');
    }
    


    /**
     * Display the specified resource.
     */
    public function show( $id)
    {
        $author = DB::table('author')->find($id);

        return view('author.detail', ['author'=>$author]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit( $id)
    {
        $author = DB::table('author')->find($id);

        return view ('author.edit', ['author' => $author]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id, request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'nationality' => 'required',
           
        ]);

        //updatenya
        DB::table('author')
              ->where('id', $id )
              ->update(
                [
                    'name' => $request->input('name'),
                    'author' => $request->input('author'),
                    
                ]);
        return redirect('/author');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy( $id)
    {
        DB::table('author')->where('id', '=', $id)->delete();

        return redirect('/author');
    }
}
