<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;
use App\Models\Book;
use File;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware ('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $book=Book::all();
        return view('book.tampil', ['book'=>$book]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $author = Author::all();

        return view('book.tambah', ['author'=>$author]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'author_id' => 'required',
            'title' => 'required',
            'summary' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg',
           
        ]);
        $imageName = time().'.'.$request->image->extension();

        $request->image->move(public_path('image'), $imageName);

        book::create([
            'title' => $request->input('title'),
            'summary' => $request->input('summary'),
            'image' => $imageName,
            'author_id' => $request->input('author_id'),
        ]);
        return redirect('/book');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $book = book::find($id);
        return view('book.detail',['book'=>$book]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $book = book::find($id);
        $author=author::get();

        return view('book.edit',['book'=>$book, 'author'=>$author]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,  $id)
    {
        $request->validate([
            'author_id' => 'required',
            'title' => 'required',
            'summary' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg',
            
            
        ]);

        $book = book::find($id);
        if($request->has('image')){
            $path='image/';
            File::delete ($path. $book->image);
            $imageName = time().'.'.$request->image->extension();

            $request->image->move(public_path('image'), $imageName);
            $book->image=$imageName;
            
        }

        $book->title =$request->input('title');
        $book->summary =$request->input('summary');
        $book->author_id=$request->input('author_id');

        $book->save();

        return redirect('/book');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $book = book::find($id);
    
        $book->delete();

        return redirect('/book');
    }
}
