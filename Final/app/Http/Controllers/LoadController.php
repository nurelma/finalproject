<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Book;
use App\Models\Load;



class LoadController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $load=Load::all();
        return view('load.tampil', ['load'=>$load]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $book = Book::all();
        $users = user::all();

        return view('load.tambah', ['book'=>$book],  ['users'=>$users] );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'users_id' => 'required',
            'book_id' => 'required',
           
           
        ]);

        load::create([
            'users_id' => $request->input('users_id'),
            'book_id' => $request->input('book_id'),
        ]);
        return redirect('/load');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $load = load::find($id);
        return view('load.detail',['load'=>$load]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $load = load::find($id);
        $users=user::get();
        $book=book::get();

        return view('load.edit',['load'=>$load, 'users'=>$users, 'book'=>$book]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'users_id' => 'required',
            'book_id' => 'required',
            
        ]);
            

        $load->users_id=$request->input('users_id');
        $load->book_id=$request->input('book_id');

        $load->save();

        return redirect('/load');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $load = load::find($id);
    
        $load->delete();

        return redirect('/load');
    }
}
